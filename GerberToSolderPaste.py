import argparse
from gerber import load_layer
from gerber import primitives
from gerber import layers
from gerber.render import GerberCairoContext, RenderSettings
from gerber.utils import convex_hull
import math
from gerber.layers import load_layer_data
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from symbol import except_clause
from _tracemalloc import start



class CustomLayer(layers.PCBLayer):
    
    def __init__(self, primitives, bounds):

        self.primitives = primitives
        self.bounds = bounds
        
    def bounds(self):
        return self.bounds

# Read gerber and Excellon files

class Gerber2GCode(object):
    
    grb_data = None
    copper_layer = None#top_copper = load_layer("/home/martin/SolderPasteProject/test.gbr")
    
    # Rendering context
    ctx = None
    
    droplets = []
    drop_size = 0.6
    drop_min_distance = 0.8
    drop_volume = 1
    placement_repeats = 3

    
    
    gcode_preamble = """M302 P1 ; allow cold extrusion
G21 ; set units to millimeters
G90 ; absolute Positioning
M83 ; extruder relative positioning
"""
    
    

    z_travel_height = "3"
    z_drop_lift_height = "1"
    feed_move = "1000" # mm/min
    feed_drop_up = "20" #feedrate for lifting needle after drop
    
    # Create SVG image
    
    
    def __init__(self, grb_string):
        self.grb_data = grb_string
        self.ctx = GerberCairoContext()
        self.copper_layer = load_layer_data(grb_string)
        self.copper_layer.layer_class = "top"
        
    @classmethod
    def fromString(cls, grb_data):
        return cls(grb_data)
     
    @classmethod
    def fromfilename(cls, filename):
        data = open(filename).read()
        return cls(data)
        
    #top_copper.render(ctx, '/home/martin/SolderPasteProject/composite.svg')
    #ctx.dump("/home/martin/SolderPasteProject/cairo_example2.svg")
    
    def render_paste(self, cam_source):
        for drop in self.droplets:
            #print(drop)
            circle = primitives.Circle(drop['position'], self.drop_size)
            cam_source.primitives.append(circle)
            
    
    def  get_placement_points(self):
        points = []
        for prim in self.copper_layer.primitives:
            if isinstance(prim, primitives.Rectangle) or isinstance(prim, primitives.Circle):
                points.append((prim.position[0],prim.position[1]))
            if isinstance(prim, primitives.Region):
                posx = (prim.bounding_box[0][1]+prim.bounding_box[0][0])/2
                posy = (prim.bounding_box[1][1]+prim.bounding_box[1][0])/2
                points.append((posx,posy))
        hull = convex_hull(points)
        #print("Hull:", hull)
        min_dist = 900000000000
        max_dist = -1
        low_left_point = None
        up_right_point = None
        
        for point in hull:
            dist = math.sqrt((point[0])**2 + (point[1])**2)
            if dist  < min_dist:
                min_dist = dist
                low_left_point = point
                
        for point in points:
            dist = math.sqrt((point[0])**2 + (point[1])**2)
            if dist  > max_dist:
                max_dist = dist
                up_right_point = point
                
        return low_left_point, up_right_point
    
    
    def render_placement(self, cam_source):
        # get lower left primitive and upper right primitive
#         points = []
#         for prim in self.copper_layer.primitives:
#             if isinstance(prim, primitives.Rectangle) or isinstance(prim, primitives.Circle):
#                 points.append((prim.position[0],prim.position[1]))
#             if isinstance(prim, primitives.Region):
#                 posx = (prim.bounding_box[0][1]+prim.bounding_box[0][0])/2
#                 posy = (prim.bounding_box[1][1]+prim.bounding_box[1][0])/2
#                 points.append((posx,posy))
#         hull = convex_hull(points)
#         #print("Hull:", hull)
#         min_dist = 900000000000
#         max_dist = -1
#         low_left_point = None
#         up_right_point = None
#         
#         for point in hull:
#             dist = math.sqrt((point[0])**2 + (point[1])**2)
#             if dist  < min_dist:
#                 min_dist = dist
#                 low_left_point = point
#                 
#         for point in points:
#             dist = math.sqrt((point[0])**2 + (point[1])**2)
#             if dist  > max_dist:
#                 max_dist = dist
#                 up_right_point = point
                
        #print("point:", point)
        
        
        low_left_point, up_right_point = self.get_placement_points()
    
        circle = primitives.Circle(low_left_point, 2)
        
        cam_source.primitives.append(circle)
        circle = primitives.Circle(up_right_point, 2)
        cam_source.primitives.append(circle)
            
            
    
    def compute_droplets(self):
        prims = self.copper_layer.primitives
        #droplets.append({"position":(155,-88),"size":10})
        for prim in prims:
            if isinstance(prim, primitives.Rectangle):
                #size = prim.width * prim.height * size_factor
                #position = prim.position
                self.fill_rect(prim)
            elif isinstance(prim, primitives.Region):
                self.fill_region(prim)
#                 print("Boundingbox:", prim.bounding_box)
#                 width = prim.bounding_box[0][1]-prim.bounding_box[0][0]
#                 height = (prim.bounding_box[1][1]-prim.bounding_box[1][0])
#                  
#                 posx = (prim.bounding_box[0][1]+prim.bounding_box[0][0])/2
#                 posy = (prim.bounding_box[1][1]+prim.bounding_box[1][0])/2
#                   
#                  
#                 region = primitives.Rectangle((posx,posy),width,height)
#                 print("posx",posx,"posy",posy, "region", region)
#                 self.fill_rect(region)
#                 print(prim.primitives)
#             if isinstance(prim, primitives.Circle):
#                 self.fill_circle(prim)

                
            
    def droplets_to_gcode(self):
        gcode = ""+ self.gcode_preamble
        print(self.droplets)
        for drop in self.droplets:
            x = str(round(drop['position'][0],4))
            y = str(round(drop['position'][1],4))
            
            gcode += "G0 Z" + str(self.z_travel_height) + " F" + str(self.feed_move) + "\n"# lift z for movement
            gcode += "G0 X" + x + " Y" + y  + "\n"# move to position
            gcode += "G0 Z0\n"  # lower z for dispension
            gcode += "G1 E" + str(self.drop_volume) + "\n"
            gcode += "G1 Z" + str(self.z_drop_lift_height) + " F" + str(self.feed_drop_up) + "\n"
            
        gcode += "G0 Z5 F" + str(self.feed_move) + "\nG0 X0 Y0"
        return gcode
    
    def placement_to_gcode(self, start_at_zero=False):
        gcode = "" + self.gcode_preamble
        low_left_point, up_right_point = self.get_placement_points()
        low_left_point_x = str(round(low_left_point[0],4))
        low_left_point_y = str(round(low_left_point[1],4))
        up_right_point_x = str(round(up_right_point[0],4))
        up_right_point_y = str(round(up_right_point[1],4))
        
        gcode += "G0 Z" + str(self.z_travel_height) + " F" + str(self.feed_move) + "\n"# lift z for movement
        
        if start_at_zero:
            gcode += "G0 X" + low_left_point_x + " Y" +  low_left_point_y + "\n"# move to position
            gcode += "G0 Z0\n"  # lower z 
            gcode += "G0 Z1 F6\n"# lift z for movement
            gcode += "G0 Z" + str(self.z_travel_height) + " F" + str(self.feed_move) + "\n"# lift z for movement
        else:
            gcode += "G92 X" +  low_left_point_x + " Y" + low_left_point_y + " Z0"
        
        for i in range(self.placement_repeats+1):
            gcode += "G0 X" + up_right_point_x + " Y" +  up_right_point_y + "\n"# move to position
            gcode += "G0 Z0\n"  # lower z 
            gcode += "G0 Z1 F6\n"# lift z for movement
            gcode += "G0 Z" + str(self.z_travel_height) + " F" + str(self.feed_move) + "\n"# lift z for movement
            gcode += "G0 X" + low_left_point_x + " Y" +  low_left_point_y + "\n"# move to position
            gcode += "G0 Z0\n"  # lower z 
            gcode += "G0 Z1 F6\n"# lift z for movement
            gcode += "G0 Z" + str(self.z_travel_height) + " F" + str(self.feed_move) + "\n"# lift z for movement
        
        return gcode
        

    def fill_rect(self, region):
        x0 = region.position[0] - region.width/2
        y0 = region.position[1] + region.height/2
        row_num = int(region.height / self.drop_min_distance)
        column_num = int(region.width / self.drop_min_distance)
        if row_num == 0:
            row_num = 1
        if column_num == 0:
            column_num = 1
        print("Rows",row_num, "Columns", column_num)
            
        row_height = region.height / row_num
        column_width = region.width / column_num
        
        print("RowsH",row_height, "ColumnsW", column_width)
        
        x0 = x0 - column_width/2
        y0 = y0 + row_height/2
        
        for i in range(1,row_num+1):
            for j in range(1,column_num+1):
                self.droplets.append({"position":(x0+j*column_width, y0-i*row_height), "amount" : self.drop_size})
                
                
    
    
    
    def in_region(self, pos, region):
        vertices = []
        for prim in region.primitives:
            if isinstance(prim, primitives.Line):
                vertices.append(prim.start)
        polygon = Polygon(vertices)
        point = Point(pos)
        #print(polygon.contains(point))
        return polygon.contains(point)
                
            
    
    # todo: rectangle around region and fill acording to rotation
    def fill_region(self, region):
        width = region.bounding_box[0][1]-region.bounding_box[0][0]
        height = (region.bounding_box[1][1]-region.bounding_box[1][0])
         
        posx = (region.bounding_box[0][1]+region.bounding_box[0][0])/2
        posy = (region.bounding_box[1][1]+region.bounding_box[1][0])/2
        
        x0 = posx - width/2
        y0 = posy + height/2
        row_num = int(height / self.drop_min_distance)
        column_num = int(width / self.drop_min_distance)
        if row_num == 0:
            row_num = 1
        if column_num == 0:
            column_num = 1
        
            
        row_height = height / row_num
        column_width = width / column_num
        
        
        x0 = x0 - column_width/2
        y0 = y0 + row_height/2
        
        for i in range(1,row_num+1):
            for j in range(1,column_num+1):
                pos =(x0+j*column_width, y0-i*row_height)
                if self.in_region(pos, region):
                    self.droplets.append({"position":pos, "amount" : self.drop_size})
    
    
        
    
    def fill_circle(self, circle):
        dia = circle.diameter
        drops_per_dia = int(dia / self.drop_min_distance)
        self.droplets.append({"position":circle.position, "amount" : self.drop_size})
        
        
    
    def clear(self):
        self.cam_source=None
        self.cam_source_placement=None
        self.copper_layer=None
        self.grb_data=None
        self.ctx = GerberCairoContext()
        self.paste_layer =None
        self.placement_layer = None


    def to_gcode(self):
        
        self.compute_droplets()

        return self.droplets_to_gcode()

    def save_gcode_to_file(self, filename):
        with open(args.output_gcode_file, 'w') as f:
            f.write(self.to_gcode())
    
    def get_preview(self, drops=False, placement=False, filename=None):
        
        self.ctx.render_layer(self.copper_layer)
        
        
        if placement:
            cam_source_placement = None
            settings_placement = RenderSettings(color=(0.8,0.0,1), alpha=0.6)
            cam_source_placement = CustomLayer([], self.copper_layer.bounds)
            self.render_placement(cam_source_placement)
            placement_layer = layers.PCBLayer(None, "topsilk", cam_source_placement)
            self.ctx.render_layer(placement_layer, settings=settings_placement, verbose=True)
                
                
        if drops: 
            settings = RenderSettings(color=(0.2,0.0,1), alpha=1)
            cam_source = None

            cam_source = CustomLayer([], self.copper_layer.bounds)
            
            self.droplets.clear()
            self.compute_droplets()
            self.render_paste(cam_source)
            paste_layer = layers.PCBLayer(None, "toppaste", cam_source)
            self.ctx.render_layer(paste_layer, settings=settings, verbose=True)
            
            
        if filename is not None:
            self.ctx.dump(filename, verbose=True)
        else:
            self.ctx.surface.finish()
            self.ctx.surface_buffer.flush()
            self.ctx.surface_buffer.seek(0)
            output = self.ctx.surface_buffer.read()
            self.ctx = GerberCairoContext()
            return output.decode("utf-8").replace("#","%23")
        
#     def __del__(self):
#         del self.ctx
#         del self.cam_source
#         del self.cam_source_placement
#         del self.copper_layer

    def convert_to_gcode(self, settings=None, preview = False, drops=False, placement=False, start_at_zero=False):
        if settings is not None:
            try:
                self.drop_size = float(settings["drop_size"])
                self.drop_volume = float(settings["drop_volume"])
                self.drop_min_distance = float(settings["drop_distance"])
                self.feed_drop_up = float(settings["feed_drop_up"])
                self.feed_move = float(settings["feed_travel"])
                self.z_travel_height = float(settings["travel_height"])
                self.z_drop_lift_height = float(settings["drop_lift_height"])
                self.placement_repeats = int(settings["placement_repeats"])
               
            except Exception as e:
                # raise
                return {"error":"error with settings"+str(e)}
            
        
        
        output = {}
        
        gcode = self.to_gcode()
        output.update({"gcode":gcode})
        
        if preview:
            prev = self.get_preview(drops=False, placement=False)
            output.update({"preview": prev})
        if drops:
            prev_drops = self.get_preview(drops=True, placement=False)
            output.update({"preview_drops": prev_drops})
        if placement:
            if preview:
                prev_placement = self.get_preview(drops=False, placement=True)
                output.update({"preview_placement": prev_placement})
            gcode = self.placement_to_gcode(start_at_zero)
            output.update({"gcode_placement": gcode})
            
        
        return output
        


if __name__ == "__main__":
    # execute only if run as a script
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('gerber_file', help='foo help')
    parser.add_argument('output_gcode_file', help='foo help')
    parser.add_argument('-p', '--preview', help='foo help')
    parser.add_argument('--placement_gcode', help='foo help')
    args = parser.parse_args()
    
    copper_layer = load_layer(args.gerber_file)
    copper_layer.layer_class = "top"
    
    grb2gcode = Gerber2GCode(args.gerber_file)
    
    print(grb2gcode.get_preview(filename=None))
    
