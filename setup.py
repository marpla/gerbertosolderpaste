import setuptools


setuptools.setup(
    name="GerberToSolderPaste",
    version="0.0.1",
    author="Martin Plaas",
    author_email="martin.plaas@gmail.com",
    description="Generate GCode for a solder paste dispenser (e.g. modified 3D printer) from a PCB Gerber file.",
    url="https://gitlab.com/marpla/GerberToSolderPaste",
    py_modules=['GerberToSolderPaste'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    setup_requires=[
        'wheel',
      ],
    install_requires=[
        'pcb-tools',
        'shapely'
      ],
)
